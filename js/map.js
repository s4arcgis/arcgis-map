var layer, legend, map, visible = [];
var identifyTask, identifyParams, thisLayerCode = null;

require([
  "esri/map",
  "esri/dijit/Print",
  "esri/tasks/PrintTemplate",
  "esri/dijit/Search",
  "esri/arcgis/utils",
  "esri/geometry/Extent",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/dijit/BasemapGallery",
  "esri/dijit/Legend",

  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",

  "esri/InfoTemplate",
  "esri/tasks/IdentifyTask",
  "esri/tasks/IdentifyParameters",
  "esri/dijit/Popup",

  "dojo/_base/Color",
  "dojo/_base/array",

  "dijit/layout/BorderContainer",
  "dijit/layout/ContentPane",

  "dojo/ready","dojo/parser","dojo/dom", "dojo/on", "dojo/query",
  "dojo/_base/array", "dojo/dom-construct", "dojo/domReady!"
], function(
   Map, Print, PrintTemplate, Search, arcgisUtils, Extent,
   ArcGISDynamicMapServiceLayer, BasemapGallery, Legend, SimpleFillSymbol,
   SimpleLineSymbol, InfoTemplate, IdentifyTask, IdentifyParameters, Popup, Color, array,
   BorderContainer, ContentPane, ready, parser, dom, on, query, arrayUtils,
   domConstruct
 ) {

   ready(function () {
     // Parse DOM nodes decorated with the data-dojo-type attribute
     parser.parse();

     //define basemap gallery
     var basemapGallery = new BasemapGallery({
       showArcGISBasemaps: true,
       map: map
     }, "basemapGallery");
     basemapGallery.startup();
   });

   //define map's starting focus area
   var extentInitial = new Extent({
     "xmin":-14988995.49860594,
     "ymin":3355420.4827545965,
     "xmax":-9177335.364028964,
     "ymax":6143843.274597085,
     "spatialReference":{"wkid":102100}
   });

   //define information popup window
   var infoPopup = new Popup({
     fillSymbol: new SimpleFillSymbol(
       SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(
         SimpleLineSymbol.STYLE_SOLID, new Color([100, 0, 50]), 2
       ),new Color([200, 88, 0, 0.25]))
     }, domConstruct.create("div"));

     //create new map
     map = new Map("cpCenter", {
       basemap : "satellite",
       extent : extentInitial,
       infowindow: infoPopup
     });

     //define layer service
     var arcGDMSLUrl = "http://darcgcit.ad.brown.edu:6080/arcgis/rest/services/demo/MapServer";
     layer = new ArcGISDynamicMapServiceLayer(arcGDMSLUrl, { opacity: 0.55 });

     layer.on("load", buildLayerList);
     map.addLayer(layer);

     legend = new Legend({
       map: map, layerInfos: [{
         "layer": layer
       }]
     },"divLegend");

     //create identify tasks and setup parameters
     identifyTask = new IdentifyTask(arcGDMSLUrl);

     identifyParams = new IdentifyParameters();
     identifyParams.mapExtent = map.extent;
     identifyParams.tolerance = 3;
     identifyParams.returnGeometry = true;

     identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
     identifyParams.width = map.width;
     identifyParams.height = map.height;

     if(layer.visibleLay){
       identifyParams.layerIds = layer.layerInfos[layer.visibleLayers];
     } else {
       identifyParams.layerIds = '';
     }

     map.on("click", executeIdentifyTask);

     //build layers drop-down list
       function buildLayerList() {
         var items = arrayUtils.map(layer.layerInfos, function(info, index) {
           return "<option class='list_item'" + "' id='" + info.id + "'' /><label for='" + info.id + "'>" + info.name + "</label>";
         });
         var ll = dom.byId("layer_list");
         ll.innerHTML = items.join(' ');
         ll.innerHTML = "<option class='list_item'" + "' id='" + 'default' + "'' />"+ ll.innerHTML;
         layer.setVisibleLayers(visible);
         on(ll, "change", updateLayerVisibility);
       }

       //provide change layer action for layer list selection
       function updateLayerVisibility() {
         var inputs = query(".list_item");
         var input;
         visible = [];

         arrayUtils.forEach(inputs, function(input) {
           if (input.selected && input.id!='default') {
             visible.push(input.id);
           }
         });
         //if there aren't any layers visible set the array to be -1
         if (visible.length === 0) {
           visible.push(-1);
         }

         layer.setVisibleLayers(visible);

         var visLayer = layer.layerInfos[layer.visibleLayers];
         if(visible[0]!='-1'){
         legend.refresh([{
            "layer": layer, "title": visLayer.name
          }]);} else{
            legend.refresh([{
            "layer": layer
            }]);
         }
       }

       function executeIdentifyTask (event) {
         identifyParams.geometry = event.mapPoint;
         identifyParams.mapExtent = map.extent;
         identifyParams.layerIds = layer.visibleLayers;

         var inputs = query(".list_item");
         var input;
         var selectedLayer=[];

         arrayUtils.forEach(inputs, function(input) {
           if (input.selected && input.id!='default') {
             selectedLayer.push(input);
           }
         });
         //if there aren't any layers visible set the array to be -1
         if (selectedLayer.length === 0) {
           selectedLayer.push(-1);
         }

         // build an array of layers by their fields and names
         var layerCodes;
         layerCodes = [
           {code:'PNHWHT10', name:'10 TRCT % white, non-Hispanic*'},
           {code:'PNHBLK10', name:'10 TRCT % black, non-Hispanic*'},
           {code:'PNTV10', name:'10 TRCT % Native American race'},
           {code:'PHAW10', name:'10 TRCT % Hawaiian race'},
           {code:'PASIAN10', name:'10 TRCT % Asian and Pacific Islander race'},
           {code:'PHISP10', name:'10 TRCT % Hispanic'},
           {code:'PKOREA10', name:'10 TRCT % Korean birth/race'},
           {code:'PJAPAN10', name:'10 TRCT % Japanese birth/race'},
           {code:'PCHINA10', name:'10 TRCT % Chinese birth/race'},
           {code:'PINDIA10', name:'10 TRCT % Indian birth/race'}
         ];

         var deferred = identifyTask
           .execute(identifyParams)
           .addCallback(function (response) {
             // response is an array of identify result objects
             // Let's return an array of features.
             return arrayUtils.map(response, function (result) {
               var feature = result.feature;
               var layerName = result.layerName;

               feature.attributes.layerName = layerName;
               layerCodes.forEach(function(item,index,array){
                 if (layerName == item.name){
                   thisLayerCode = item.code;
                 }
               });

               //if(selectedLayer[0][0] > 0){
                 var someInfoTemplate = new InfoTemplate("${TRACT} - ${COUNTY}, ${STATE}",
                 "${" + thisLayerCode + "}");
                 feature.setInfoTemplate(someInfoTemplate);
               /*}
               else{
                 var someInfoTemplate = new InfoTemplate("",
                 "no layer selected");
               }*/
               return feature;
             });
           });
           map.infoWindow.setFeatures([deferred]);
           map.infoWindow.show(event.mapPoint);
         }

      //search bar
      var dijitSearch = new Search({
       map: map,
       autoComplete: true,
       arcgisGeocoder: {
         suffix: " Redlands, CA"
       }
     }, "divSearch");
      dijitSearch.startup();

        //printing service
        //setting up the printer function

        var myLayouts = [{
         "name": "Letter ANSI A Landscape",
         "label": "Landscape (PDF)",
         "format": "pdf",
         "options": {
                   "legendLayers": [], // empty array means no legend
                   "scalebarUnit": "Miles",
                   "titleText": "Landscape PDF"
                 }
               }, {
                 "name": "Letter ANSI A Portrait",
                 "label": "Portrait (JPG)",
                 "format": "jpg",
                 "options": {
                   "legendLayers": [],
                   "scaleBarUnit": "Miles",
                   "titleText": "Portrait JPG"
                 }
               }];


           /*
            * Step: create the print templates
            */
            var myTemplates = [];
            dojo.forEach(myLayouts, function (lo) {
             var t = new PrintTemplate();
             t.layout = lo.name;
             t.label = lo.label;
             t.format = lo.format;
             t.layoutOptions = lo.options;
             t.preserveScale = false;
             myTemplates.push(t);
           });


            var widgetPrint = new Print({
             map: map,
             url: "http://darcgcit.AD.Brown.Edu:6080/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task",
             templates: myTemplates
           }, divPrint);
            widgetPrint.startup();


          });
